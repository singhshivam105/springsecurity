package com.shivam.springsecurity.demo.controller;

import javax.annotation.processing.Generated;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping("/showMyLoginPage")
	public String showMyLoginPage() {
		
		return "fancy-login";
	}
	
	@GetMapping("/access-denied")
	public String showMyAccessDeniedPage(){
		
		return "access-denied";
	}
}
