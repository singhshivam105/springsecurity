<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title>Superbuddy Homepage yoooo</title>
</head>
<body>
<h2>Superbuddy HomePage</h2>
<hr>
On the main page

<p>
Users: <security:authentication property="principal.username"/>
<br><br>
Roles: <security:authentication property="principal.authorities"/>


</p>

<hr>


<security:authorize access="hasRole('Manager')">
<!-- Add alink for the leaders page -->

<a href="${pageContext.request.contextPath }/leaders" >Leaders Meeting</a>
only for leader

</security:authorize>
<hr>



<!-- Add alink for the systems/admins page -->

<security:authorize access="hasRole('Admin')">
<a href="${pageContext.request.contextPath }/systems" >It Systems  Meeting</a>
only for Admin

</security:authorize>
<!-- add logout -->

<form:form action="${pageContext.request.contextPath}/logout" method="POST">

<input type="submit" value="logout"/>

</form:form>

</body>


</html>